object DbgMsgForm: TDbgMsgForm
  Left = 200
  Top = 107
  Width = 432
  Height = 572
  Caption = 'Debug Messenger'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = '�l�r �o�S�V�b�N'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = True
  OnCloseQuery = FormCloseQuery
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object Memo: TTntMemo
    Left = 0
    Top = 0
    Width = 416
    Height = 513
    Align = alClient
    Color = clBlack
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'FixedSys'
    Font.Style = []
    ImeName = 'MSIME95'
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object MainMenu1: TMainMenu
    Left = 144
    object File1: TMenuItem
      Caption = '&File'
      object miSave: TMenuItem
        Caption = '&Save'
        ShortCut = 16467
        OnClick = miSaveClick
      end
      object miSaveAs: TMenuItem
        Caption = 'Save &As...'
        OnClick = miSaveAsClick
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object miOpenLogFile: TMenuItem
        Caption = 'Open saved &Log file'
        OnClick = miOpenLogFileClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = Exit1Click
      end
    end
    object Edit1: TMenuItem
      Caption = '&Edit'
      object Copy1: TMenuItem
        Caption = '&Copy'
        ShortCut = 16429
        OnClick = Copy1Click
      end
      object Clear1: TMenuItem
        Caption = 'Clea&r'
        ShortCut = 8219
        OnClick = Clear1Click
      end
      object Allcopy1: TMenuItem
        Caption = '&All copy'
        ShortCut = 16449
        OnClick = Allcopy1Click
      end
    end
    object View1: TMenuItem
      Caption = '&View'
      OnClick = View1Click
      object ViewStringItem: TMenuItem
        Caption = '&String'
        Checked = True
        OnClick = ViewStringItemClick
      end
      object Dump1: TMenuItem
        Caption = '&Dump'
        Checked = True
      end
      object FuncIn1: TMenuItem
        Caption = 'Func &In'
      end
      object FuncOut1: TMenuItem
        Caption = 'Func &Out'
      end
      object miBuffering: TMenuItem
        Caption = '&Buffering mode'
        Checked = True
        OnClick = miBufferingClick
      end
      object miAcceptCommand: TMenuItem
        Caption = '&Accept Command'
        Checked = True
      end
      object miTimestamp: TMenuItem
        Caption = '&With Timestamp'
        Checked = True
        OnClick = miTimestampClick
      end
      object miWindowHandle: TMenuItem
        Caption = 'With Window &Handle'
        OnClick = miWindowHandleClick
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object miTopMost: TMenuItem
        Caption = '&Top Most'
        OnClick = miTopMostClick
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object miVersionUp: TMenuItem
        Caption = '&Version Up'
        OnClick = miVersionUpClick
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object miChangeWindowTitle: TMenuItem
        Caption = 'Change Window &Title'
        OnClick = miChangeWindowTitleClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object About1: TMenuItem
        Caption = '&About...'
        OnClick = About1Click
      end
    end
  end
  object BufferTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = BufferTimerTimer
    Left = 88
    Top = 64
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'txt'
    FileName = '*.txt'
    Filter = 'Text|*.txt'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 8
    Top = 8
  end
end
