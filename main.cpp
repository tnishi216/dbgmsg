//---------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop
#include <vcl\inifiles.hpp>
#include <tchar.h>
#include <mmsystem.h>
#include <stdio.h>

#pragma link "TntStdCtrls"

#define	tchar	TCHAR

#include "main.h"
#include "ctrque.h"

#define	QUEBUF_SIZE		256
//TODO: ここを大きくしても画面更新処理時にまとめて行うため、
// VCLとは異なるthreadで処理できないと意味がない

#define	LOG_FILENAME	"backup.txt"

bool download_app(const tchar *appname);
HANDLE WinExecEx( const tchar *cmd, int show, const tchar *dir=NULL, const tchar *title=NULL );
bool GetRegKey( HKEY Key, const tchar *SubKey, tchar *RetData, long maxlen );

// thread unsafe
wchar_t *AnsiToUTF16(const char *ansi, int len);

class TRelease {
	void *HeapMem;
public:
	TRelease()
		:HeapMem(NULL){}
	TRelease(void *heap_mem)
		:HeapMem(heap_mem){}
	void reset(void *mem){ HeapMem = mem; }
	~TRelease()
		{ if (HeapMem) delete[] HeapMem; }
};

//---------------------------------------------------------------------------
#pragma resource "*.dfm"
TDbgMsgForm *DbgMsgForm;
TIniFile *IniFile;
bool Buffering = true;
//---------------------------------------------------------------------------
__fastcall TDbgMsgForm::TDbgMsgForm(TComponent* Owner)
	: TForm(Owner)
{
	ViewString = true;

	IniFile = new TIniFile( ExtractFilePath( Application->ExeName ) + "DBGMSG.INI" );
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::FormCloseQuery(TObject *Sender,
	  bool &CanClose)
{
	SaveProfile();
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::miSaveClick(TObject *Sender)
{
	Save(false);
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::miSaveAsClick(TObject *Sender)
{
	Save(true);
}
void __fastcall TDbgMsgForm::miOpenLogFileClick(TObject *Sender)
{
	AnsiString filename = ExtractFilePath( Application->ExeName ) + LOG_FILENAME;
	ShellExecute( NULL, _T("open"), filename.c_str(), NULL, NULL, SW_SHOW );
}
//---------------------------------------------------------------------------
void TDbgMsgForm::Save(bool q)
{
	if (q || FileName.IsEmpty()){
		if (!SaveDialog->Execute())
			return;
		FileName = SaveDialog->FileName;
	}
	Memo->Lines->SaveToFile(FileName);
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::Exit1Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

#define	USE_MSGDATA	1

#if USE_MSGDATA
struct TMsgData {
	char *msg_s;
	wchar_t *msg_w;
	DWORD time;
	HWND hwnd;
	TMsgData()
	{
		msg_s = NULL;
		msg_w = NULL;
		time = 0;
		hwnd = NULL;
	}
	TMsgData(char *_msg, DWORD _time, HWND _hwnd)
	{
		msg_s = _msg;
		msg_w = NULL;
		time = _time;
		hwnd = _hwnd;
	}
	TMsgData(wchar_t *_msg, DWORD _time, HWND _hwnd)
	{
		msg_s = NULL;
		msg_w = _msg;
		time = _time;
		hwnd = _hwnd;
	}
	TMsgData(int null)
	{
		msg_s = NULL;
		msg_w = NULL;
		time = 0;
		hwnd = NULL;
	}
#if 0
	~TMsgData()
	{
		if (msg_s) delete[] msg_s;
		if (msg_w) delete[] msg_w;
	}
#endif
};
ctrque::TQueue<TMsgData> que( QUEBUF_SIZE );
#else
ctrque::TQueue<tchar*> que( QUEBUF_SIZE );
ctrque::TQueue<DWORD> timeque( QUEBUF_SIZE );
ctrque::TQueue<HWND> hwndque( QUEBUF_SIZE );
#endif
char *newstr( const char *data, int len )
{
	char *p = new char[ len+1 ];
	if ( !p )
		return NULL;
	memcpy( p, data, len * sizeof(char) );
	p[len] = '\0';
	return p;
}
wchar_t *newstr( const wchar_t *data, int len )
{
	wchar_t *p = new wchar_t[ len+1 ];
	if ( !p )
		return NULL;
	memcpy( p, data, len * sizeof(wchar_t) );
	p[len] = '\0';
	return p;
}
char *newstr( const char *data )
{
	int len = strlen( data );
	char *p = new char[ len+1 ];
	if ( !p )
		return NULL;
	memcpy( p, data, len * sizeof(char) );
	p[len] = '\0';
	return p;
}
void __fastcall TDbgMsgForm::PutBuffer( char *str, DWORD now, HWND hwnd )
{
#if USE_MSGDATA
	if ( !que.Put( TMsgData( str, now, hwnd ) ) ){
		BufferTimerTimer( this );
		que.Put( TMsgData( str, now, hwnd ) );
	}
	if ( !BufferTimer->Enabled )
		BufferTimer->Enabled = true;
#else
	if ( !que.Put( str ) ){
		BufferTimerTimer( this );
		que.Put( str );
	}
	timeque.Put(now);
	hwndque.Put(hwnd);
	if ( !BufferTimer->Enabled )
		BufferTimer->Enabled = true;
#endif
}
void __fastcall TDbgMsgForm::PutBuffer( wchar_t *str, DWORD now, HWND hwnd )
{
#if USE_MSGDATA
	if ( !que.Put( TMsgData( str, now, hwnd ) ) ){
		BufferTimerTimer( this );
		que.Put( TMsgData( str, now, hwnd ) );
	}
	if ( !BufferTimer->Enabled )
		BufferTimer->Enabled = true;
#else
	if ( !que.Put( str ) ){
		BufferTimerTimer( this );
		que.Put( str );
	}
	timeque.Put(now);
	hwndque.Put(hwnd);
	if ( !BufferTimer->Enabled )
		BufferTimer->Enabled = true;
#endif
}
void __fastcall TDbgMsgForm::WMCopyData(TMessage &Message)
{
	DWORD now = timeGetTime();
	AnsiString s;
	WideString w;
	if ( Message.LParam ){
		COPYDATASTRUCT &cds = *(COPYDATASTRUCT*)Message.LParam;
		switch ( cds.dwData ){
			case 1:	// as string
			case 3:	// as string without CR
				if ( !ViewString ) return;
				if ( Buffering ){
					PutBuffer( newstr( (const tchar *)cds.lpData, cds.cbData ), now, (HWND)Message.WParam );
					return;
				} else {
					if ( cds.lpData ){
						tchar *p = newstr( (const tchar *)cds.lpData, cds.cbData );
						s = p;
						delete p;
					} else {
						s = _T("(null)");
					}
				}
				break;
			case 2: // as dump(byte)
				{
				tchar buf1[80], buf2[80];
				tchar *p1 = buf1;
				tchar *p2 = buf2;
				for ( int i=0;i<(int)cds.cbData;i+=8 ){
					for ( int j=0;j<8;j++ ){
						char c = ((char*)cds.lpData)[i+j];
						wsprintf( p1, _T("%02X "), (unsigned char)c );
						p1 += 3;
						wsprintf( p2, _T("%c"), (unsigned char)c >= ' ' ? c : '.' );
						p2++;
					}
				}
				*p1 = '\0';
				*p2 = '\0';
				if ( Buffering ){
					int len1 = _tcslen(buf1);
					int len2 = _tcslen(buf2);
					tchar *newbuf = new tchar[ len1 + len2 + 4 ];
					tchar *p = newbuf;
					memcpy( p, buf1, len1 * sizeof(tchar) );
					p += len1;
					memcpy( p, _T(" : "), 3 * sizeof(tchar) );
					p += 3;
					memcpy( p, buf2, len2 * sizeof(tchar) );
					p[len2] = '\0';
					PutBuffer( newbuf, now, (HWND)Message.WParam );
					return;
				} else {
					s = AnsiString(buf1) + _T(" : ") + buf2;
				}
				}
				break;
			case 5:	// as unicode string
			case 6:	// as unicode string without CR
				if ( !ViewString ) return;
				if ( Buffering ){
					PutBuffer( newstr( (const wchar_t *)cds.lpData, cds.cbData/sizeof(wchar_t) ), now, (HWND)Message.WParam );
					return;
				} else {
					if ( cds.lpData ){
						wchar_t *p = newstr( (const wchar_t *)cds.lpData, cds.cbData/sizeof(wchar_t) );
						w = p;
						delete p;
					} else {
						s = _T("(null)");
					}
				}
				break;
			case 0x10:	// View ON
				ViewString = true;
				return;
			case 0x11:	// View OFF
				ViewString = false;
				return;
			case 0x12:	// Save&Clear
				Clear1Click(this);
				break;
			default:
				if ( Buffering ){
					PutBuffer( newstr( _T("(unknown data type)") ), now, (HWND)Message.WParam );
					return;
				} else {
					s = _T("(unknown data type)");
				}
				break;
		}
	} else {
		s = _T("(illegal parameter)");
	}
	if (miAcceptCommand->Checked){
		if (s[0]=='!'){
			wchar_t *wp = AnsiToUTF16(s.c_str()+1, s.Length()-1);
			if (ParseCommand(wp)){
				return;
			}
		}
	}
	if (s.data()){
		AddLine(s.c_str(), s.Length(), now, (HWND)Message.WParam);
	} else
	if (w.c_bstr()){
		AddLine(w.c_bstr(), now, (HWND)Message.WParam);
	}
}
bool TDbgMsgForm::ParseCommand(const wchar_t *s)
{
	static const wchar_t *prefix = L"<CMD>:";
	const int prefix_len = 6;
	if (wcsncmp(s, prefix, prefix_len)){
		return false;
	}
	s += 6;
	if (!wcscmp(s, L"clear")){
		Memo->Clear();
	}
	return true;
}
void __fastcall TDbgMsgForm::Copy1Click(TObject *Sender)
{
	if (Memo->Lines->Count>0){
		Memo->CopyToClipboard();
	}
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::FormShow(TObject *Sender)
{
	LoadProfile();
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::FormHide(TObject *Sender)
{
	SaveProfile();
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::Clear1Click(TObject *Sender)
{
	if (Memo->Lines->Count>0){
		Memo->CopyToClipboard();
		AnsiString filename = ExtractFilePath( Application->ExeName ) + LOG_FILENAME;
		Memo->Lines->SaveToFile(filename);
	}
	Memo->Clear();	
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::ViewStringItemClick(TObject *Sender)
{
	ViewString = !ViewString;
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::View1Click(TObject *Sender)
{
	ViewStringItem->Checked = ViewString;
	miBuffering->Checked = Buffering;	
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::Allcopy1Click(TObject *Sender)
{
	Memo->SelectAll();
	Memo->CopyToClipboard();
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::miBufferingClick(TObject *Sender)
{
	Buffering = !Buffering;
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::BufferTimerTimer(TObject *Sender)
{
	//
	if ( que.GetCount() > 0 ){
		Memo->Lines->BeginUpdate();
		do {
#if USE_MSGDATA
			TMsgData msg = que.Get();
			wchar_t *p;
			if (msg.msg_w){
				p = msg.msg_w;
			} else {
				// Ansi -> Unicode
				p = AnsiToUTF16(msg.msg_s, -1);
			}
#else
			tchar *p = que.Get();
			DWORD now = timeque.Get();
			HWND hwnd = hwndque.Get();
#endif
			if (miAcceptCommand->Checked){
				if (p[0]=='!'){
					if (ParseCommand(p+1)){
						delete p;
						p = NULL;
					}
				}
			}
			if (p){
#if USE_MSGDATA
				AddLine( p, msg.time, msg.hwnd );
				if (msg.msg_w) delete[] msg.msg_w;
				if (msg.msg_s) delete[] msg.msg_s;
#else
				AddLine( p, now, hwnd );
				delete[] p;
#endif
			}
		} while ( que.GetCount() > 0 );
		Memo->SelStart = 0x7FFFFFFF;
		Memo->Lines->EndUpdate();
		::SendMessage( Memo->Handle, EM_SCROLLCARET, 0, 0 );
		BufferTimer->Enabled = false;
	}
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::miTimestampClick(TObject *Sender)
{
	miTimestamp->Checked = !miTimestamp->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::miWindowHandleClick(TObject *Sender)
{
	miWindowHandle->Checked = !miWindowHandle->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::miTopMostClick(TObject *Sender)
{
	miTopMost->Checked = !miTopMost->Checked;	
	if ( miTopMost->Checked ){
		SetWindowPos( Handle, HWND_TOPMOST, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE );
	} else {
		SetWindowPos( Handle, HWND_NOTOPMOST, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE );
	}
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::miVersionUpClick(TObject *Sender)
{
	if (MessageBox(Handle, "Download the latest version?", NULL, MB_YESNO)!=IDYES)
		return;
	download_app("dbgmsg");
	Close();
}
//---------------------------------------------------------------------------
void __fastcall TDbgMsgForm::miChangeWindowTitleClick(TObject *Sender)
{
	miChangeWindowTitle->Checked = !miChangeWindowTitle->Checked;
	if (miChangeWindowTitle->Checked){
		Caption = "Debug Messanger";
	} else {
		Caption = "Debug Messenger";
	}
}
//---------------------------------------------------------------------------
void TDbgMsgForm::LoadProfile()
{
	Left = IniFile->ReadInteger( _T("Main"), _T("Left"), Left );
	Top = IniFile->ReadInteger( _T("Main"), _T("Top"), Top );
	Width = IniFile->ReadInteger( _T("Main"), _T("Width"), Width );
	Height = IniFile->ReadInteger( _T("Main"), _T("Height"), Height );
	miAcceptCommand->Checked = IniFile->ReadInteger(_T("Common"), _T("AcceptCommand"), miAcceptCommand->Checked);
	miTimestamp->Checked = IniFile->ReadInteger(_T("View"), _T("WithTimestamp"), miTimestamp->Checked);
}
void TDbgMsgForm::SaveProfile()
{
	WINDOWPLACEMENT wp;
	wp.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement( Handle, &wp );
	RECT &rc = wp.rcNormalPosition;
	IniFile->WriteInteger( _T("Main"), _T("Left"), rc.left );
	IniFile->WriteInteger( _T("Main"), _T("Top"), rc.top );
	IniFile->WriteInteger( _T("Main"), _T("Width"), rc.right - rc.left );
	IniFile->WriteInteger( _T("Main"), _T("Height"), rc.bottom - rc.top );
	IniFile->WriteInteger(_T("Common"), _T("AcceptCommand"), miAcceptCommand->Checked);
	IniFile->WriteInteger(_T("View"), _T("WithTimestamp"), miTimestamp->Checked);
}
void TDbgMsgForm::AddLine(const char *s, int len, DWORD now, HWND hwnd)
{
	AddLine( AnsiToUTF16(s, len), now, hwnd );
}
void TDbgMsgForm::AddLine(const wchar_t *s, DWORD now, HWND hwnd)
{
	if (miTimestamp->Checked || miWindowHandle->Checked){
		wchar_t temp[40];
		wchar_t *p = temp;
		if (miTimestamp->Checked){
			swprintf(p, L"%d.%03d:", now/1000, now%1000);
			p += wcslen(p);
		}
		if (miWindowHandle->Checked){
			swprintf(p, L"%08X:", hwnd);
			//p += strlen(p);
		}
		WideString t = temp;
		t += s;
		Memo->Lines->Add( t );
	} else {
		Memo->Lines->Add( s );
	}
}
void __fastcall TDbgMsgForm::About1Click(TObject *Sender)
{
	char msg[256];
	wsprintf(msg, "Build: %s", __DATE__);
	MessageBox(Handle, msg, "", MB_OK);
}
//---------------------------------------------------------------------------
bool download_app(const tchar *appname)
{
	char key[256];
	AnsiString filename;

	filename += "http://pdic.sakura.ne.jp/cgi-bin/download/download.cgi?file=";
	filename += appname;
	int show = SW_SHOW;
	if ( (UINT)ShellExecute( NULL, _T("open"), filename.c_str(), NULL, NULL, show ) <= 32 ){
		// もしエラーがでれば
		key[0] = '\0';
		if ( GetRegKey( HKEY_CLASSES_ROOT, _T(".htm"), key, sizeof(key) ) ){
			_tcscat( key, _T("\\shell\\open\\command") );
			if ( GetRegKey( HKEY_CLASSES_ROOT, key, key, sizeof(key) ) ){
				tchar *p = _tcsstr( key, _T("\"%1\"") );
				if ( !p ){
					p = _tcsstr( key, _T("%1") );
					if ( p )
						memmove( p-1, p+2, strlen(p+2)+1 );
				} else
					memmove( p-1, p+4, strlen(p+4)+1 );
				_tcscat( key, _T(" ") );
				_tcscat( key, filename.c_str() );
				return WinExecEx( key, show ) ? true : false;
			}
		}
	}
	else return true;
	return false;
}

static const tchar *MyWinTitle = _T("<Processing...>");

HANDLE WinExecEx( const tchar *cmd, int show, const tchar *dir, const tchar *title )
{
#ifdef WINCE
	PROCESS_INFORMATION pi;
	if ( !CreateProcess( NULL, (LPTSTR)cmd, NULL, NULL, FALSE, 0, NULL, NULL, NULL, &pi ) )
		return NULL;
	return pi.hProcess;
#else
	STARTUPINFO sui;
	memset( &sui, 0, sizeof(STARTUPINFO) );
	sui.cb = sizeof(STARTUPINFO);
	sui.dwFlags = STARTF_USESHOWWINDOW;
	sui.wShowWindow = (WORD)show;
	sui.lpTitle = (LPTSTR)(title ? title : MyWinTitle);
	PROCESS_INFORMATION pi;
	if ( !CreateProcess( NULL, (LPTSTR)cmd, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, dir, &sui, &pi ) )
		return NULL;
	return pi.hProcess;
#endif
}

bool GetRegKey( HKEY Key, const tchar *SubKey, tchar *RetData, long maxlen )
{
	HKEY hk;

	int Result = RegOpenKeyEx( Key, SubKey, 0, KEY_QUERY_VALUE, &hk) == ERROR_SUCCESS;

	if ( Result ){
#ifdef WINCE
		DWORD type = REG_SZ;
		RegQueryValueEx( hk, NULL, NULL, &type, (LPBYTE)RetData, (DWORD*)&maxlen );
#else
		RegQueryValue(hk, NULL, RetData, &maxlen);
#endif
		RegCloseKey(hk);
	}
	return Result;
}

wchar_t *utf16 = NULL;
int utf16len = 0;
TRelease utf16_rel;
wchar_t *AnsiToUTF16(const char *ansi, int len)
{
	if (len<0){
		len = strlen(ansi);
	}
	if (utf16len<len){
		if (utf16) delete[] utf16;
		utf16 = new wchar_t[len+1];
		utf16len = len;
		utf16_rel.reset(utf16);
	}
	len = MultiByteToWideChar(CP_ACP, 0, ansi, len, utf16, utf16len);
	utf16[len] = '\0';
	return utf16;
}

