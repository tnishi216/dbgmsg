//---------------------------------------------------------------------------
#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Menus.hpp>
#include <vcl\ExtCtrls.hpp>
#include <Dialogs.hpp>
#include "TntStdCtrls.hpp"
//---------------------------------------------------------------------------
class TDbgMsgForm : public TForm
{
__published:	// IDE 管理のコンポーネント
	TMainMenu *MainMenu1;
	TMenuItem *File1;
	TMenuItem *Exit1;
	TTntMemo *Memo;
	TMenuItem *Edit1;
	TMenuItem *Copy1;
	TMenuItem *View1;
	TMenuItem *ViewStringItem;
	TMenuItem *Dump1;
	TMenuItem *FuncIn1;
	TMenuItem *FuncOut1;
	TMenuItem *Help1;
	TMenuItem *About1;
	TMenuItem *Clear1;
	TMenuItem *Allcopy1;
	TMenuItem *miBuffering;
	TTimer *BufferTimer;
	TMenuItem *miTopMost;
	TMenuItem *miSave;
	TMenuItem *N1;
	TSaveDialog *SaveDialog;
	TMenuItem *miSaveAs;
	TMenuItem *miAcceptCommand;
	TMenuItem *N2;
	TMenuItem *miVersionUp;
	TMenuItem *miTimestamp;
	TMenuItem *N3;
	TMenuItem *miChangeWindowTitle;
	TMenuItem *miWindowHandle;
	TMenuItem *N4;
	TMenuItem *miOpenLogFile;
	TMenuItem *N5;
	void __fastcall Exit1Click(TObject *Sender);
	void __fastcall Copy1Click(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);


	void __fastcall Clear1Click(TObject *Sender);
	
	void __fastcall ViewStringItemClick(TObject *Sender);
	void __fastcall View1Click(TObject *Sender);
	void __fastcall Allcopy1Click(TObject *Sender);
	void __fastcall miBufferingClick(TObject *Sender);
	
	void __fastcall BufferTimerTimer(TObject *Sender);
	void __fastcall miTopMostClick(TObject *Sender);
	void __fastcall miSaveClick(TObject *Sender);
	void __fastcall miSaveAsClick(TObject *Sender);
	void __fastcall miVersionUpClick(TObject *Sender);
	void __fastcall miTimestampClick(TObject *Sender);
	void __fastcall miChangeWindowTitleClick(TObject *Sender);
	void __fastcall miWindowHandleClick(TObject *Sender);
	void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
	void __fastcall miOpenLogFileClick(TObject *Sender);
	void __fastcall About1Click(TObject *Sender);
private:	// ユーザー宣言
	AnsiString FileName;
protected:
	void Save(bool q);
	bool ParseCommand(const wchar_t *s);
	bool ViewString;
	void __fastcall WMCopyData(TMessage &Message);
	void __fastcall PutBuffer( char *str, DWORD now, HWND hwnd );
	void __fastcall PutBuffer( wchar_t *str, DWORD now, HWND hwnd );
	BEGIN_MESSAGE_MAP
		MESSAGE_HANDLER( WM_COPYDATA, TMessage, WMCopyData )
	END_MESSAGE_MAP( TForm )
public:		// ユーザー宣言
	__fastcall TDbgMsgForm(TComponent* Owner);
	void LoadProfile();
	void SaveProfile();
	void AddLine(const wchar_t *s, DWORD now, HWND hwnd);
	void AddLine(const char *s, int len, DWORD now, HWND hwnd);
};
//---------------------------------------------------------------------------
extern TDbgMsgForm *DbgMsgForm;
//---------------------------------------------------------------------------
#endif

