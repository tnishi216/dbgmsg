//---------------------------------------------------------------------------
#ifndef ctrqueH
#define ctrqueH
//---------------------------------------------------------------------------
namespace ctrque {
template <class T>
class TQueue {
protected:
	T *queue;
	T *putpos;
	T *getpos;
	T *bottom;
	int count;
	int size;
protected:
	void NextPutPos()
	{
		if ( putpos == bottom ) putpos = &queue[0];
		else putpos++;
	}
	void NextGetPos()
	{
		if ( getpos == bottom ) getpos = &queue[0];
		else getpos++;
	}
public:
	TQueue( int size );
	virtual ~TQueue();
	bool Put( T data );
	T Get( );
	void Clear();
	int GetCount()
		{ return count; }
};
template <class T>
TQueue<T>::TQueue( int _size )
{
	queue = new T[ _size ];
	putpos = getpos = &queue[0];
	bottom = &queue[_size-1];
	count = 0;
	size = _size;
}
template <class T>
TQueue<T>::~TQueue()
{
	delete[] queue;
}
template <class T>
void TQueue<T>::Clear()
{
	putpos = getpos = &queue[0];
	count = 0;
}
template <class T>
bool TQueue<T>::Put( T data )
{
	if ( count >= size ) return false;
	*putpos = data;
	count++;
	NextPutPos();
	return true;
}
template <class T>
T TQueue<T>::Get( )
{
	if ( count == 0 ) return NULL;

	T data = *getpos;
	NextGetPos();
	count--;
	return data;
}
}
#endif

